# TP - Ingenieria
*Evaluación de tecnologías y Framework BDD*

<br>

**Framework**<br>
Electron<br>
https://electron.atom.io


**IDE**<br>
Atom<br>
https://atom.io


**Framework Testing BDD**<br>
Mocha<br>
https://mochajs.org


**Complemento para integrar mocha con Atom**<br>
Atom-Mocha | por boogie666 (importante por que hay muchos paquetes con el mismo nombre)<br>
https://atom.io/packages/atom-mocha (se puede instalar directamente desde el package manager de atom en preferences > packages)


**Servidor local**<br>
NodeJS<br>
https://nodejs.org


**Librería de assertions**<br>
ShouldJS<br>
https://github.com/shouldjs/should.js
