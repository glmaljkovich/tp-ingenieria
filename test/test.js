var assert = require('assert');
var should = require('should');

function Perro(nombre, edad){
	this.nombre = nombre;
	this.edad 	= edad;
  this.edadPerruna = function(){
    return this.edad * 7
  }
	this.ladrar = function(){
		return "Woof!"
	}
}


describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });

    it('should return 0 when the value is not present', function() {
      assert.equal(0, [1,2,3].indexOf(1));
    });
  });
});

describe('Perro de 5 años llamado firulais', function() {
  let perritou = new Perro("Firulais", 5);

  it('Debe llamarse Firulais (Sino me engañaron)', function() {
      perritou.should.have.property('nombre', "Firulais");
  });

  it('Debe tener 5 años (Sino me estan verseando mucho)', function() {
      perritou.edad.should.equal(5);
  });

  describe('#edadPerruna()', function() {
    it('deberia retornar 35 años', function() {
      perritou.edadPerruna().should.equal(35);
    });
  });

  describe('#ladrar()', function() {
    it('deberia retornar "Woof!"', function() {
      perritou.ladrar().should.be.instanceOf(String).and.equal("Woof!");
    });
  });
});
